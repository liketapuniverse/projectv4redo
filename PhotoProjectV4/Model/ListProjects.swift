//
//  ListProjects.swift
//  PhotoProjectV4
//
//  Created by admin on 22/10/2021.
//

import UIKit
import Alamofire
import SVProgressHUD

class ListProjects {
    static var shared = ListProjects()
    var projects: [Project] = []
    init() {
      getProjectsList()
    }
    
    func getProjectsList() {
        if let listCodableProjects = readSavedProjectsList() {
            projects = listCodableProjects.map() {Project(id: $0.id, name: $0.name)}
            NotificationCenter.default.post(name: NSNotification.Name(AppConstants.didLoadProjects), object: nil)
        } else {
            SVProgressHUD.show()
            AF.request("\(AppConstants.endPoint)/xproject", method: .get).responseJSON { response in
                guard let json = response.value as? [String: Any],
                      let pros = json["projects"] as? [[String: Any]]
                else { return}
                for i in 0..<pros.count {
                    guard let name = pros[i]["name"] as? String,
                          let id = pros[i]["id"] as? Int
                    else { return }
                    let project = Project(id: String(id), name: name)
                    self.projects.append(project)
                }
                SVProgressHUD.dismiss()
                NotificationCenter.default.post(name: NSNotification.Name(AppConstants.didLoadProjects), object: nil)
            }
        }
        
    }
    
    func saveProjectsList() {
        let listCodableProjects = projects.map() {CodableProject(id: $0.id, name: $0.name)}
        guard let data = try? JSONEncoder().encode(listCodableProjects) else { return }
        let encodedString = String(data: data, encoding: .utf8)
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileUrl = dir.appendingPathComponent(AppConstants.projectsListFilename)
            try? encodedString?.write(to: fileUrl, atomically: false, encoding: .utf8)
        }
    }
    
    func readSavedProjectsList() -> [CodableProject]? {
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        let fileUrl = dir.appendingPathComponent(AppConstants.projectsListFilename)
        guard let jsonString = try? String(contentsOf: fileUrl, encoding: .utf8),
              let dataFromString = jsonString.data(using: .utf8),
              let projectsList = try? JSONDecoder().decode([CodableProject].self, from: dataFromString) else { return nil}
        return projectsList
    }
}
