//
//  ProjectDetail.swift
//  PhotoProjectV4
//
//  Created by admin on 22/10/2021.
//

import UIKit
import Alamofire
import SVProgressHUD

class ProjectDetail {
    
}

class Project {
    let id: String
    let name: String
    var listPhotos: [PhotoData] = []
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func getProjectDetail(_ id: String, completion: @escaping (PhotoData)-> Void) {
        listPhotos = []
        if UserDefaults.standard.bool(forKey: "didCache-\(self.id)") {
            guard let listCodablePhoto = getListCodablePhoto() else {
                return
            }
            for photo in listCodablePhoto {
                if photo.projectId == self.id {
                    guard let toAddPhoto = PhotoData.load(fileName: photo.imageId,photo: photo) else { continue}
                    completion(toAddPhoto)
                }
            }
        } else {
            SVProgressHUD.show()
            AF.request("\(AppConstants.endPoint)/xprojectdetail", method: .post, parameters: ["id": Int(id)]).responseJSON { response in
                guard let json = response.value as? [String: Any],
                      let photos = json["photos"] as? [[String: Any]]
                                        else {return}
                SVProgressHUD.show()
                var count = 0
                for photo in photos {
                    guard let url = photo["url"] as? String,
                          let frame = photo["frame"] as? [String: Any],
                          let x = frame["x"] as? Double,
                          let y = frame["y"] as? Double,
                          let width = frame["width"] as? Double,
                          let height = frame["height"] as? Double
                    else {continue}
                    AF.request(url).response { response in
                        count += 1
                        if count == photo.count {
                            SVProgressHUD.dismiss()
                        }
                        guard let data = response.data else { return }
                        if let image = UIImage(data: data) {
                            let photoData = PhotoData(id: UUID().uuidString, image: image, frame: CGRect(x: x, y: y, width: width, height: height), transform: .identity)
                            
                            completion(photoData)
                        }
                    }
                }
            }
        }
    }
    
    func saveProject() {
        var listCodablePhoto: [CodablePhotoData] = []
        for i in 0..<listPhotos.count {
            let photo = listPhotos[i]
            let codablePhotoData = CodablePhotoData(imageId: photo.id, projectId: self.id,rect: photo.frame, transform: photo.transform)
            listCodablePhoto.append(codablePhotoData)
            let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileURL = url.appendingPathComponent("\(photo.id).jpg")
            if let data = photo.image.jpegData(compressionQuality:  1.0),
               !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    try data.write(to: fileURL)
                    print("file saved--\(fileURL.path)")
                } catch {
                    print("error saving file:", error)
                }
            }
        }
        guard let data = try? JSONEncoder().encode(listCodablePhoto) else { return  }
        let encodedString = String(data: data, encoding: .utf8)!
        print("encode \(encodedString)")
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent("listPhoto\(self.id).json")
            do {
                try encodedString.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch let error {
                print(error)
            }
        }
        
        UserDefaults.standard.set(true, forKey:"didCache-\(self.id)")
        ListProjects.shared.saveProjectsList()
    }
    
    func getListCodablePhoto() -> [CodablePhotoData]? {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent("listPhoto\(self.id).json")
            if let jsonString = try? String(contentsOf: fileURL, encoding: .utf8) {
                if let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false) {
                    do {
                        let listPhoto = try JSONDecoder().decode([CodablePhotoData].self, from: dataFromString)
                        return listPhoto
                        
                    } catch {
                        print(error)
                        return nil
                    }
                }
            }
        }
        return nil
    }
}

class CodableProject: Codable {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

class PhotoData {
    let id: String
    let image: UIImage
    var frame: CGRect
    var transform: CGAffineTransform
    var alpha: Double = 1
    init(id: String, image: UIImage, frame: CGRect, transform: CGAffineTransform) {
        self.id = id
        self.image = image
        self.frame = frame
        self.transform = transform
    }
    
    static func load(fileName: String, photo: CodablePhotoData) -> PhotoData?{
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = url.appendingPathComponent("\(fileName).jpg")
        
        guard let image = UIImage(contentsOfFile: fileURL.path) else { return nil
        }
        return PhotoData(id: photo.imageId, image: image, frame: photo.rect, transform: photo.transform)
    }
    
    func save(_ view: CustomImageView) {
        let t = view.transform
        transform = CGAffineTransform(a: t.a, b: t.b, c: t.c, d: t.d, tx: t.tx, ty: t.ty)
        view.transform = .identity
        frame = CGRect(x: view.frame.minX, y: view.frame.minY, width: view.frame.width, height: view.frame.height)
        view.transform = t
        alpha = view.alpha
    }
}

class CodablePhotoData: Codable {
    let imageId, projectId: String
    let rect: CGRect
    let transform: CGAffineTransform
    init(imageId:  String, projectId: String, rect: CGRect,transform: CGAffineTransform) {
        self.imageId = imageId
        self.projectId = projectId
        self.rect = rect
        self.transform = transform
    }
}
