//
//  PhotoCollectionViewCell.swift
//  PhotoProjectV4
//
//  Created by tu on 10/25/21.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    override func draw(_ rect: CGRect) {
        layer.borderWidth = 0
        layer.borderColor = UIColor.blue.cgColor
        layer.masksToBounds = true
    }
    override var isSelected: Bool {
        didSet {
            layer.borderWidth = isSelected ? 2 : 0
        }
    }
    @IBOutlet weak var imageView: UIImageView!
}
