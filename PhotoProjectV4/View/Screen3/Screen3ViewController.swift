//
//  Screen3ViewController.swift
//  PhotoProjectV4
//
//  Created by tu on 10/25/21.
//

import UIKit
import Photos

protocol CustomPhotoPickerDelegate: AnyObject {
    func imageDidPicked(_ image: UIImage)
}

class Screen3ViewController: UIViewController {

    @IBOutlet weak var albumMenuButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    let numberItemPerRow = 3
    let itemSpacing: Double = 10
    let fetchOptions = PHFetchOptions()
    var listAlbums = [Album]()
    var listAssetAlbums = [PHFetchResult<PHAsset>]()
    var currentAssetAlbum = PHFetchResult<PHAsset>()
    weak var delegate: CustomPhotoPickerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        photoCollectionView.delegate = self
        photoCollectionView.dataSource = self
        photoCollectionView.allowsMultipleSelection = true
        addButton.setTitleColor(.blue, for: .normal)
        addButton.setTitleColor(.gray, for: .disabled)
        addButton.isEnabled = false
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchAlbum {
            DispatchQueue.main.async {
                self.photoCollectionView.reloadData()
                self.getAlbumTitles()
            }
        }
    }
    
    func fetchAlbum(completion: @escaping ()-> Void) {
        let access = PHPhotoLibrary.authorizationStatus()
        if access != .authorized {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == .authorized) {
                    self.fetchOnAuthorized(completion: completion)
                } else if (newStatus == .denied){
                    self.alertOnPermissionDenied()
                }
            })
        } else if access == .denied {
            self.alertOnPermissionDenied()
        } else {
            fetchOnAuthorized(completion: completion)
        }
    }
    
    func fetchOnAuthorized(completion: @escaping ()-> Void) {
        let recents = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        listAlbums.append(Album(name: "recents", index: 0))
        listAssetAlbums.append(recents)
        currentAssetAlbum = recents
        
        let albumsCollection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
        for i in 0..<albumsCollection.count{
            if let name = albumsCollection[i].localizedTitle{
                let fetchedAlbum = PHAsset.fetchAssets(in: albumsCollection[i], options: fetchOptions)
                listAlbums.append(Album(name: name, index: i + 1))
                listAssetAlbums.append(fetchedAlbum)
            }
        }
        completion()
    }
    
    func alertOnPermissionDenied() {
        let alert = UIAlertController(title: "Allow access", message: "Please allow acess to photo so we can add new photo for you", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            AppUtils.gotoAppPrivacySettings()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.navigationController?.popViewController(animated: false)
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getAssetThumbnailOrFullImage(asset: PHAsset, isFullSize: Bool? = false) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        let targetSize = (isFullSize ?? false) ? PHImageManagerMaximumSize : CGSize(width: 200, height: 200)
        manager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func albumIndexDidChange(_ index: Int, completion: @escaping (String)-> Void) {
        currentAssetAlbum = listAssetAlbums[index]
        completion(listAlbums[index].name)
    }
    
    func getAlbumTitles() {
        if #available(iOS 14.0, *) {
            let listElement : [UIMenuElement] = listAlbums.map { album in
                UIAction(title: album.name) { _ in
                    self.albumIndexDidChange(album.index) { name in
                        DispatchQueue.main.async {
                            self.albumMenuButton.setTitle(name, for: .normal)
                            self.photoCollectionView.reloadData()
                        }
                    }
                }
            }
            albumMenuButton.setTitle(listAlbums.first?.name, for: .normal)
            albumMenuButton.menu = UIMenu(title: "List album", image: UIImage(systemName: "sun.max"), identifier: nil, options: .displayInline, children: listElement)
            albumMenuButton.showsMenuAsPrimaryAction = true
        }
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        if let listIndexPath = photoCollectionView.indexPathsForSelectedItems {
            for indexPath in listIndexPath {
                if let d = delegate {
                    let asset = currentAssetAlbum.object(at: indexPath.row)
                    let fullImage = getAssetThumbnailOrFullImage(asset: asset, isFullSize: true)
                    d.imageDidPicked(fullImage)
                }
            }
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension Screen3ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentAssetAlbum.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = photoCollectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as? PhotoCollectionViewCell else { return UICollectionViewCell()}
        cell.imageView.image = getAssetThumbnailOrFullImage(asset: currentAssetAlbum.object(at: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + CGFloat((numberItemPerRow - 1))*flowLayout.minimumInteritemSpacing
        let size = (collectionView.bounds.width - totalSpace)/CGFloat(numberItemPerRow)
        print("size \(size)")
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        checkAddButtonEnabled()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        checkAddButtonEnabled()
    }
    
    func checkAddButtonEnabled() {
        if let listIndexPath = photoCollectionView.indexPathsForSelectedItems {
            addButton.isEnabled = listIndexPath.count > 0
        }
    }
}

class Album {
    let name: String
    let index: Int
    init(name: String, index: Int) {
        self.name = name
        self.index = index
    }
}
