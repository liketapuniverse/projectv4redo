//
//  ProjectTableViewCell.swift
//  PhotoProjectV4
//
//  Created by admin on 21/10/2021.
//

import UIKit
protocol ProjectTableViewCellDelegate: AnyObject {
    func deleteCell(_ cell: ProjectTableViewCell)
}

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var projectContainerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var projectNameLabel: UILabel!
    weak var delegate: ProjectTableViewCellDelegate?
    var beginX: CGFloat = 0
    var didLoad = false
    override func draw(_ rect: CGRect) {
        if didLoad { return }
        projectContainerTrailingConstraint.constant = 0
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        addGestureRecognizer(panGesture)
        didLoad = true
    }
    
    @objc func didPan(_ sender: UIPanGestureRecognizer) {
        if sender.state == .began {
            beginX = projectContainerTrailingConstraint.constant
        }
        if sender.state == .changed {
            let pannedDistance = sender.translation(in: self).x
            projectContainerTrailingConstraint.constant = beginX - pannedDistance
        }
        if sender.state == .ended || sender.state == .cancelled {
            var limitedBorder = projectContainerTrailingConstraint.constant
            limitedBorder = min(50, max(0, limitedBorder)) // 0 - 12 - 50
            limitedBorder = limitedBorder < 25 ? 0 : 50 // 0 - 50
            projectContainerTrailingConstraint.constant = limitedBorder
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func deleteProject(_ sender: Any) {
        if let d = delegate {
            d.deleteCell(self)
        }
    }
}
 
