//
//  Screen2ViewController.swift
//  PhotoProjectV4
//
//  Created by admin on 22/10/2021.
//

import UIKit

class Screen2ViewController: UIViewController {
    var project: Project!
    @IBOutlet weak var galleryView: GalleryView!
    @IBOutlet weak var editView: EditView!
    @IBOutlet weak var sliderView: SliderView!
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryView.clipsToBounds = true
        galleryView.delegate = self
        editView.clipsToBounds = true
        sliderView.delegate = self
        galleryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideGallery)))
        project.getProjectDetail(project.id) { photoData in
            self.galleryView.project = self.project
            self.galleryView.presentPhoto(photoData) 
        }
        
    }
    
    @objc func didTapOutsideGallery() {
        galleryView.deleteButton.isHidden = true
        editView.setLayersHidden(true)
        sliderView.isHidden = true
    }
    @IBAction func onBack(_ sender: Any) {
        let alert = UIAlertController(title: "Save project", message: "Save project?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { _ in
            self.project.saveProject()
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Don't save", style: .cancel, handler: { _ in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func exportPhoto(_ sender: Any) {
        galleryView.exportPhoto()
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        guard let vc = UIStoryboard(name: "Screen3", bundle: nil).instantiateViewController(withIdentifier: "Screen3ViewController") as? Screen3ViewController else { return }
        vc.delegate = self
        self.present(vc, animated: true)
    }
}

extension Screen2ViewController: GalleryViewDelegate {
    func imageRemoved() {
        editView.setLayersHidden(true)
    }
    
    func imageSelected(_ image: UIImageView) {
        editView.addBorderForView(image)
        sliderView.isHidden = false
        sliderView.value = image.alpha
        sliderView.setNeedsDisplay()
        
    }
}

extension Screen2ViewController: CustomPhotoPickerDelegate {
    func imageDidPicked(_ image: UIImage) {
        galleryView.addPhoto(image)
    }
}


extension Screen2ViewController: CustomImageViewOpacityDelegate {
    func imageOpacityChange(_ value: Double) {
        galleryView.updateImageOpacity(value)
    }
}
