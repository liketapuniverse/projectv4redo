//
//  GalleryView.swift
//  PhotoProjectV4
//
//  Created by tu on 10/24/21.
//

import UIKit
import Photos

protocol GalleryViewDelegate: AnyObject {
    func imageSelected(_ image: UIImageView)
    func imageRemoved()
}

class GalleryView: UIView {
    var project: Project!
    var listPhotos: [PhotoData] = []
    var listId: [String]  = []
    var selected: Int = -1
    var beginT: CGAffineTransform = .identity
    var beginPoint: CGPoint = .zero
    let deleteButton = CustomButton()
    var didLoad = false
    var currentSelectedImage: CustomImageView? = nil
    weak var delegate: GalleryViewDelegate?
    
    override func draw(_ rect: CGRect) {
        if didLoad { return }
        let buttonSize: Int = 20
        deleteButton.frame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        deleteButton.layer.cornerRadius = CGFloat(buttonSize/2)
        deleteButton.backgroundColor = .red
        let centerWhiteView = UIView()
        centerWhiteView.frame = CGRect(x: 0, y: 0, width: 8, height: 2)
        centerWhiteView.backgroundColor = .white
        centerWhiteView.center = deleteButton.convert(deleteButton.center, from:deleteButton)
        deleteButton.addSubview(centerWhiteView)
        addSubview(deleteButton)
        
        deleteButton.isHidden = true
        deleteButton.addTarget(self, action: #selector(handleDeletePhoto), for: .touchUpInside)
        didLoad = true
    }
    
    @objc func handleDeletePhoto() {
        currentSelectedImage?.removeFromSuperview()
        project.listPhotos.remove(at: selected)
        listId.remove(at: selected)
        deleteButton.isHidden = true
        if let d = delegate {
            d.imageRemoved()
        }
    }
    
    func presentPhoto(_ photo: PhotoData) {
        let image = CustomImageView(image: photo.image)
        image.id = photo.id
        image.frame = photo.frame
        image.transform = photo.transform
        addSubview(image)
        setUpImageView(image)
        project.listPhotos.append(photo)
        listId.append(photo.id)
    }
    
    func addPhoto(_ image: UIImage) {
        let pickedImage = CustomImageView(image: image)
        pickedImage.id = UUID().uuidString
        var width = self.bounds.width/2
        var height = width*(pickedImage.bounds.height/pickedImage.bounds.width)
        //down-scale when height exceeds parent
        if height > self.bounds.height {
            let downSizeRatio  = self.bounds.height/height
            height *= downSizeRatio
            width *= downSizeRatio
        }
        pickedImage.frame = CGRect(x: 0, y: 0, width: width, height: height)
        pickedImage.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        addSubview(pickedImage)
        setUpImageView(pickedImage)
        let photo = PhotoData(id: pickedImage.id, image: image, frame: pickedImage.frame, transform: .identity)
        project.listPhotos.append(photo)
        listId.append(photo.id)
    }
    
    func setUpImageView(_ image: CustomImageView) {
        image.isUserInteractionEnabled = true
        image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap(_:))))
        image.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan(_:))))
        image.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(didPinch(_:))))
        image.addGestureRecognizer(UIRotationGestureRecognizer(target: self, action: #selector(didRotate(_:))))
    }
    
    func notifyDelegate(_ image: CustomImageView) {
        image.superview?.bringSubviewToFront(image)
        let imageTransform = image.transform
        let scale = sqrt(Double(imageTransform.a * imageTransform.a + imageTransform.c * imageTransform.c))
        let center = image.convert(CGPoint(x: image.bounds.midX, y: -15/scale), to: self)
        deleteButton.center = center
        deleteButton.isHidden = false
        bringSubviewToFront(deleteButton)
//        let radian:Double = atan2( Double(image.transform.b), Double(image.transform.a))
//        deleteButton.transform = CGAffineTransform(rotationAngle: CGFloat(radian))
        
        //save status
        currentSelectedImage = image
        guard let i = listId.firstIndex(where: { id in
            id == image.id
        }) else { return}
        selected = i
        project.listPhotos[i].save(image)
        
        
        if let d = delegate {
            d.imageSelected(image)
        }
    }
    
    func exportPhoto() {
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            saveGalleryOnPermissionGranted()
        } else if (status == PHAuthorizationStatus.denied) {
            self.alertOnPermissionDenied()
        } else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.saveGalleryOnPermissionGranted()
                } else {
                    self.alertOnPermissionDenied()
                    
                }
            })
        }
    }
    
    func alertOnPermissionDenied() {
        let alert = UIAlertController(title: "Allow access", message: "Please allow acess to photo so we can save new photo for you", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            AppUtils.gotoAppPrivacySettings()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func saveGalleryOnPermissionGranted() {
        deleteButton.isHidden = true
        DispatchQueue.main.async {
            //remove border
           
            let image = self.makeImageFrom(CGSize(width: self.bounds.width, height: self.bounds.height))
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        
    }
    
    func updateImageOpacity(_ alpha: Double) {
        guard let image = currentSelectedImage else {return}
        image.alpha = alpha
        project.listPhotos[selected].save(image)
        
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Error saving photo", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.window?.rootViewController?.present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Photo was saved successfully", preferredStyle: .alert)
            let shareAction = UIAlertAction(title: "Share now", style: .default) { _ in
                self.shareImage(image)
            }
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            ac.addAction(shareAction)
            self.window?.rootViewController?.present(ac, animated: true)
        }
    }
    
    func shareImage(_ image: UIImage) {
        let imageToShare = [ image]
       let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
       activityViewController.popoverPresentationController?.sourceView = self// so that iPads won't crash
        self.window?.rootViewController?.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func makeImageFrom(_ size: CGSize) -> UIImage {
        let format = UIGraphicsImageRendererFormat()
//        format.scale = 5
        let size = CGSize(width: size.width, height: size.height)

        let renderer = UIGraphicsImageRenderer(size: size, format: format)
        
        let image = renderer.image { (ctx) in
            ctx.cgContext.setFillColor((UIColor(named: "galleryBackground") ?? UIColor.white).cgColor)
            ctx.fill(CGRect(x: 0, y: 0, width: self.bounds.width , height: self.bounds.height))
            
            for photo in project.listPhotos {
                //reference
//                let t = photo.transform.translatedBy(x: 200, y: 0)
//                let t = CGAffineTransform(translationX: 200, y: 0).concatenating(photo.transform)
                let t = photo.transform.concatenating(CGAffineTransform(translationX: photo.frame.midX, y: photo.frame.midY))
                ctx.cgContext.concatenate(t)
                let rect = CGRect(x: -photo.frame.width/2, y: -photo.frame.height/2, width: photo.frame.width, height: photo.frame.height)
                photo.image.draw(in: rect, blendMode: .normal, alpha: photo.alpha)
                
                ctx.cgContext.concatenate(t.inverted())
            }
        }
        return image
    }
    
    @objc func didTap(_ sender: UITapGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else { return }
        notifyDelegate(image)
    }
    
    @objc func didPan(_ sender: UIPanGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else {return}
        let translation = sender.translation(in: self)
        image.center = CGPoint(x: image.center.x + translation.x, y: image.center.y + translation.y)
        sender.setTranslation(.zero, in: self)
        notifyDelegate(image)
    }
    
    @objc func didPinch(_ sender: UIPinchGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else {return}
        image.transform = image.transform.scaledBy(x: sender.scale, y: sender.scale)
        sender.scale = 1
        notifyDelegate(image)
    }
    
    @objc func didRotate(_ sender: UIRotationGestureRecognizer) {
        guard let image = sender.view as? CustomImageView else { return }
        image.transform = image.transform.rotated(by: sender.rotation)
        sender.rotation = 0
        notifyDelegate(image)
    }
}

class CustomImageView: UIImageView {
    var id: String = ""
}

class CustomButton: UIButton {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return bounds.insetBy(dx: -10, dy: -10).contains(point)
    }
}
