//
//  AppConstants.swift
//  PhotoProjectV4
//
//  Created by admin on 22/10/2021.
//

import Foundation

class AppConstants {
    static let endPoint = "https://tapuniverse.com"
    static let didLoadProjects = "didLoadProjects"
    static let projectsListFilename = "projectsList.json"
}
