//
//  AppUtils.swift
//  PhotoProjectV4
//
//  Created by tu on 10/25/21.
//

import UIKit

class AppUtils {
    static func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url) else {
                assertionFailure("Not able to open App privacy settings")
                return
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
